# Operating Systems

- Krishn Bera    201556009
- Kishan Sangani 201501053


## Implementation of  OS Shell in C
##### A feature loaded Linux shell in C which implements all the basic commands of bash as well as a few custom tweaks to the shell environment.

Run "make" (without quotes) to make a MakeFile
Then implement the C-based shell by ./shell

cd , pwd , echo , pinfo , exit are built-in commands.
Other commands are executed as the system commands.
Background process implemented.
Usage: `command-name &` or `command-name [arguments] &` or `command-name bg`

Bonus:
nightswatch
Usage: `nightswatch [options] command-name`
Options: -n seconds: Time interval to execute command periodically
Commands:
1. interrupt: output the number of times that particular CPU has been interrupted by the keyboard controller
3. dirty: print size of dirty memory
